/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.cshf.modules.activiti.dao;

import com.cshf.modules.activiti.entity.HistoryDetailEntity;
import com.cshf.modules.activiti.entity.ProcessActivityEntity;
import com.cshf.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.context.annotation.Primary;

import java.util.List;
import java.util.Map;

/**
 * @author Jone
 */
@Mapper
@Primary
public interface ProcessActivityDao extends BaseDao<ProcessActivityEntity> {

    List<ProcessActivityEntity> getMyProcessInstancePage(Map<String, Object> params);

    List<HistoryDetailEntity> getTaskHandleDetailInfo(String processInstanceId);
}
