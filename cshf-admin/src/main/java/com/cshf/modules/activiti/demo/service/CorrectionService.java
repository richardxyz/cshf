package com.cshf.modules.activiti.demo.service;


import com.cshf.common.service.CrudService;
import com.cshf.modules.activiti.demo.dto.CorrectionDTO;
import com.cshf.modules.activiti.demo.entity.CorrectionEntity;

/**
 * 转正申请
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface CorrectionService extends CrudService<CorrectionEntity, CorrectionDTO> {

    void updateInstanceId(String instanceId, Long id);
}