/**
 * Copyright (c) 2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.cshf.modules.activiti.service;

import com.cshf.common.page.PageData;
import com.cshf.common.service.BaseService;
import com.cshf.modules.activiti.dto.HistoryDetailDTO;
import com.cshf.modules.activiti.dto.ProcessActivityDTO;
import com.cshf.modules.activiti.entity.ProcessActivityEntity;

import java.util.List;
import java.util.Map;

/**
 * 流程自定义查询
 *
 * @author Jone
 */
public interface ActivitiService  extends BaseService<ProcessActivityEntity> {

    PageData<ProcessActivityDTO> getMyProcessInstancePage(Map<String, Object> params);

    List<HistoryDetailDTO> getTaskHandleDetailInfo(String processInstanceId);
}
