package com.cshf.modules.activiti.demo.dao;

import com.cshf.common.dao.BaseDao;
import com.cshf.modules.activiti.demo.entity.CorrectionEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 转正申请
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface CorrectionDao extends BaseDao<CorrectionEntity> {

    void updateInstanceId(String instanceId, Long id);
}