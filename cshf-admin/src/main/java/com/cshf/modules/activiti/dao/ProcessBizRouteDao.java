package com.cshf.modules.activiti.dao;

import com.cshf.modules.activiti.entity.ProcessBizRouteEntity;
import com.cshf.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:Jone
 */
@Mapper
public interface ProcessBizRouteDao extends BaseDao<ProcessBizRouteEntity> {
    ProcessBizRouteEntity getProcDefBizRoute(@Param("proDefId") String id);

    List<ProcessBizRouteEntity> getLatestProcDefBizRoute(@Param("procDefKey") String procDefKey);
}
