package com.cshf.modules.activiti.service.impl;

import com.cshf.common.service.impl.BaseServiceImpl;
import com.cshf.common.utils.ConvertUtils;
import com.cshf.modules.activiti.dao.ProcessBizRouteDao;
import com.cshf.modules.activiti.dto.ProcessBizRouteDTO;
import com.cshf.modules.activiti.entity.ProcessBizRouteEntity;
import com.cshf.modules.activiti.service.ProcessBizRouteService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:Jone
 */
@Service
public class ProcessBizRouteServiceImpl  extends BaseServiceImpl<ProcessBizRouteDao, ProcessBizRouteEntity> implements ProcessBizRouteService {

    @Override
    public ProcessBizRouteDTO getProcDefBizRoute(String id) {
        ProcessBizRouteEntity entity = baseDao.getProcDefBizRoute(id);
        ProcessBizRouteDTO dto = ConvertUtils.sourceToTarget(entity, ProcessBizRouteDTO.class);
        return dto;
    }

    @Override
    public void save(ProcessBizRouteDTO processBizRouteDTO) {
        ProcessBizRouteEntity entity = ConvertUtils.sourceToTarget(processBizRouteDTO, ProcessBizRouteEntity.class);
        this.insert(entity);
    }

    @Override
    public void updateProcBizRoute(ProcessBizRouteDTO processBizRouteDTO) {
        ProcessBizRouteEntity entity = ConvertUtils.sourceToTarget(processBizRouteDTO, ProcessBizRouteEntity.class);
        this.updateById(entity);
    }

    @Override
    public ProcessBizRouteDTO getLatestProcDefBizRoute(String procDefKey) {
        List<ProcessBizRouteEntity> list = baseDao.getLatestProcDefBizRoute(procDefKey);
        if(list.isEmpty()){
            return null;
        }
        ProcessBizRouteEntity entity = list.get(0);
        return ConvertUtils.sourceToTarget(entity, ProcessBizRouteDTO.class);
    }
}
