package com.cshf.modules.activiti.service;

import com.cshf.common.service.BaseService;
import com.cshf.modules.activiti.dto.ProcessBizRouteDTO;
import com.cshf.modules.activiti.entity.ProcessBizRouteEntity;

/**
 * @Author:Jone
 */
public interface ProcessBizRouteService  extends BaseService<ProcessBizRouteEntity> {

    ProcessBizRouteDTO getProcDefBizRoute(String id);

    void save(ProcessBizRouteDTO processBizRouteDTO);

    void updateProcBizRoute(ProcessBizRouteDTO processBizRouteDTO);

    ProcessBizRouteDTO getLatestProcDefBizRoute(String procDefKey);
}
