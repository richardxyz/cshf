/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.cshf.modules.tenant.service;


import com.cshf.modules.sys.dto.SysRoleDTO;
import com.cshf.modules.sys.entity.SysRoleEntity;
import com.cshf.common.page.PageData;
import com.cshf.common.service.BaseService;

import java.util.List;
import java.util.Map;


/**
 * 租户角色
 * 
 * @author Mark sunlightcs@gmail.com
 */
public interface SysTenantRoleService extends BaseService<SysRoleEntity> {

	PageData<SysRoleDTO> page(Map<String, Object> params);

	List<SysRoleDTO> list(Map<String, Object> params);

	SysRoleDTO get(Long id);

	void save(SysRoleDTO dto);

	void update(SysRoleDTO dto);

	void delete(Long[] ids);

}
