/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.cshf.modules.tenant.service;

import com.cshf.modules.tenant.dto.SysTenantDTO;
import com.cshf.modules.tenant.entity.SysTenantEntity;
import com.cshf.common.page.PageData;
import com.cshf.common.service.BaseService;

import java.util.Map;


/**
 * 租户管理
 * 
 * @author Mark sunlightcs@gmail.com
 */
public interface SysTenantService extends BaseService<SysTenantEntity> {

	PageData<SysTenantDTO> page(Map<String, Object> params);

	SysTenantDTO get(Long id);

	void save(SysTenantDTO dto);

	void update(SysTenantDTO dto);

	void delete(Long[] ids);

	SysTenantDTO getTenantCode(Long tenantCode);
}
