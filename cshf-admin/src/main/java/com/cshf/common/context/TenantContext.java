/**
 * Copyright (c) 2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.cshf.common.context;

import com.cshf.modules.security.user.UserDetail;
import com.cshf.modules.sys.enums.SuperAdminEnum;
import com.cshf.common.utils.HttpContextUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 租户
 *
 * @author Mark sunlightcs@gmail.com
 */
public class TenantContext {

    public static Long getTenantCode(UserDetail user){
        if(user.getTenantCode() == null){
            return null;
        }

        String tenantCode = HttpContextUtils.getTenantCode();
        //超级管理员，才可以切换租户
        if(user.getSuperAdmin() == SuperAdminEnum.YES.value()){
            if(StringUtils.isNotBlank(tenantCode)){
                return Long.parseLong(tenantCode);
            }
        }
        return user.getTenantCode();
    }
}
